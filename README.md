# wemirr-platform

## 重点

仓库地址为

https://gitee.com/battcn/wemirr-platform

当前仓库只是一个备份库

## 介绍

一个业余时间写的开源项目、欢迎使用和提建议、包括低码平台、常见 中台 、SAAS 、 多租户功能、最最少的代码实现功能

[演示地址](https://cloud.battcn.com/) 在线演示地址，尽量别删数据。没有做一键回滚数据
[配套前端](https://gitee.com/battcn/wemirr-platform-ui) 配套的 UI 

[Nepxion-Discovery](https://github.com/Nepxion/Discovery) 蓝绿、灰度、流量保护

[d2-curd-plus](http://greper.gitee.io/d2-crud-plus) 前端的 CRUD 

[OpenAPi3](https://springdoc.org/) Swagger 标准版

#### 软件架构

Vue、Spring Cloud Alibaba 2.2.4.RELEASE、Spring Cloud Hoxton.SR9、Nacos、Sentinel、
Nepxion、Mybatis-Plus、多租户、灰度、Oauth2.0、Spring Security、Redis、Mysql、MongoDB、
ShardingJdbc、ShardingSphere


